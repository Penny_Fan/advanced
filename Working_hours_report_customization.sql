-- customized Working Hours Report in AM/PM format using Custom Query in GDS
select 
  local_date,
  user_name,
  CASE WHEN(CAST(SUBSTRING(CAST(MIN(local_time) AS STRING),1,2) AS INTEGER)) < 12 
  THEN CONCAT(CAST(MIN(local_time) AS STRING)," ","AM") 
    WHEN (CAST(SUBSTRING(CAST(MIN(local_time) AS STRING),1,2) AS INTEGER)) >= 12 
  THEN CONCAT(CAST(((CAST(SUBSTRING(CAST(MIN(local_time) AS STRING),1,2) AS INTEGER))-12) AS STRING), SUBSTRING(CAST(MIN(local_time) AS STRING),-6,6)," PM")
  END as first_activity,
  CASE WHEN(CAST(SUBSTRING(CAST(MAX(local_time) AS STRING),1,2) AS INTEGER)) < 12 
  THEN CONCAT(CAST(MAX(local_time) AS STRING)," ","AM") 
    WHEN (CAST(SUBSTRING(CAST(MAX(local_time) AS STRING),1,2) AS INTEGER)) >= 12 
  THEN CONCAT(CAST(((CAST(SUBSTRING(CAST(MAX(local_time) AS STRING),1,2) AS INTEGER))-12) AS STRING), SUBSTRING(CAST(MAX(local_time) AS STRING),-6,6)," PM")
  END as last_activity,
  CONCAT( FLOOR(SUM(CASE
          WHEN productivity = 'PRODUCTIVE' THEN duration_sec
      END
        )/3600), 'h ', FLOOR((SUM(CASE
            WHEN productivity = 'PRODUCTIVE' THEN duration_sec
        END
          )-3600*FLOOR(SUM(CASE
              WHEN productivity = 'PRODUCTIVE' THEN duration_sec
          END
            )/3600))/60), 'm ', SUM(CASE
        WHEN productivity = 'PRODUCTIVE' THEN duration_sec
    END
      )- 3600*FLOOR(SUM(CASE
          WHEN productivity = 'PRODUCTIVE' THEN duration_sec
      END
        )/3600)- 60* FLOOR((SUM(CASE
            WHEN productivity = 'PRODUCTIVE' THEN duration_sec
        END
          )-3600*FLOOR(SUM(CASE
              WHEN productivity = 'PRODUCTIVE' THEN duration_sec
          END
            )/3600))/60), 's') AS productive,
  CONCAT( FLOOR(SUM(CASE
          WHEN productivity = 'UNPRODUCTIVE' THEN duration_sec
      END
        )/3600), 'h ', FLOOR((SUM(CASE
            WHEN productivity = 'UNPRODUCTIVE' THEN duration_sec
        END
          )-3600*FLOOR(SUM(CASE
              WHEN productivity = 'UNPRODUCTIVE' THEN duration_sec
          END
            )/3600))/60), 'm ', SUM(CASE
        WHEN productivity = 'UNPRODUCTIVE' THEN duration_sec
    END
      )- 3600*FLOOR(SUM(CASE
          WHEN productivity = 'UNPRODUCTIVE' THEN duration_sec
      END
        )/3600)- 60* FLOOR((SUM(CASE
            WHEN productivity = 'UNPRODUCTIVE' THEN duration_sec
        END
          )-3600*FLOOR(SUM(CASE
              WHEN productivity = 'UNPRODUCTIVE' THEN duration_sec
          END
            )/3600))/60), 's') AS unproductive,
  CONCAT( FLOOR(SUM(CASE
          WHEN productivity = 'UNDEFINED' THEN duration_sec
      END
        )/3600), 'h ', FLOOR((SUM(CASE
            WHEN productivity = 'UNDEFINED' THEN duration_sec
        END
          )-3600*FLOOR(SUM(CASE
              WHEN productivity = 'UNDEFINED' THEN duration_sec
          END
            )/3600))/60), 'm ', SUM(CASE
        WHEN productivity = 'UNDEFINED' THEN duration_sec
    END
      )- 3600*FLOOR(SUM(CASE
          WHEN productivity = 'UNDEFINED' THEN duration_sec
      END
        )/3600)- 60* FLOOR((SUM(CASE
            WHEN productivity = 'UNDEFINED' THEN duration_sec
        END
          )-3600*FLOOR(SUM(CASE
              WHEN productivity = 'UNDEFINED' THEN duration_sec
          END
            )/3600))/60), 's') AS undefined,
  CONCAT( FLOOR(SUM(CASE
          WHEN active_state = 'ACTIVE' THEN duration_sec
      END
        )/3600), 'h ', FLOOR((SUM(CASE
            WHEN active_state = 'ACTIVE' THEN duration_sec
        END
          )-3600*FLOOR(SUM(CASE
              WHEN active_state = 'ACTIVE' THEN duration_sec
          END
            )/3600))/60), 'm ', SUM(CASE
        WHEN active_state = 'ACTIVE' THEN duration_sec
    END
      )- 3600*FLOOR(SUM(CASE
          WHEN active_state = 'ACTIVE' THEN duration_sec
      END
        )/3600)- 60* FLOOR((SUM(CASE
            WHEN active_state = 'ACTIVE' THEN duration_sec
        END
          )-3600*FLOOR(SUM(CASE
              WHEN active_state = 'ACTIVE' THEN duration_sec
          END
            )/3600))/60), 's') AS active_time,

FROM
  `productivity-lab-291116.Labs.events`
WHERE local_date between '2021-6-1' and '2021-10-05'
GROUP BY
  1,2
ORDER BY
  1 DESC
