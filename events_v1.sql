SELECT
  time AS utc_timestamp,
  ip AS device_ip,
  publicip AS public_ip,
  email AS alarm_email_action,
  popup AS alarm_popup_action,
  screenshot AS alarm_screenshot_action,
  terminate AS alarm_terminate_action,
  webhook AS alarm_webhook_action,
  computer AS computer_name,
  computerid AS computer_id,
  primarydomain AS primary_domain,
  logondomain AS logon_domain,
  userid AS user_id,
  user,
  userraw AS username,
  useralias AS user_alias,
  titlebar,
  executable,
  log_description AS application,
  url AS full_url,
  category,
  duration AS duration_sec,

      CASE
    WHEN active_state=1 THEN 'ACTIVE'
    WHEN active_state=2 THEN 'PASSIVE'
    WHEN active_state=3 THEN 'PASSIVELOCKED'
    ELSE 'UNDEFINED'
    END AS active_state,
  
      CASE
            WHEN COALESCE(IF(websiteid IS NOT NULL, productivity, productivity), 0) = 1 THEN 'PRODUCTIVE'
            WHEN COALESCE(IF(websiteid IS NOT NULL, productivity, productivity), 0) = -1 THEN 'UNPRODUCTIVE'
        ELSE 'UNDEFINED'
        END AS productivity,

      EXTRACT(WEEK from logs.time) AS week_of_year,
      604700 AS accountid,

      CONCAT(CASE
    WHEN COALESCE(IF(websiteid IS NOT NULL, productivity, productivity), 0) = 1 THEN 'PRODUCTIVE'
    WHEN COALESCE(IF(websiteid IS NOT NULL, productivity, productivity), 0) = -1 THEN 'UNPRODUCTIVE'
    ELSE 'UNDEFINED'END,' ',CASE
    WHEN active_state=1 THEN 'ACTIVE'
    WHEN active_state=2 THEN 'PASSIVE'
    WHEN active_state=3 THEN 'PASSIVELOCKED'
    ELSE 'UNDEFINED' END) AS productivity_state,

      DATE(logs.time, 'US/Central') AS local_date,
      DATETIME(logs.time, 'US/Central') AS local_datetime,
      TIME(logs.time,'US/Central') AS local_time,
      
      CASE
          when EXTRACT(DAYOFWEEK from DATE(logs.time, 'US/Central')) = 2 then 'Monday'
          when EXTRACT(DAYOFWEEK from DATE(logs.time, 'US/Central')) = 3 then 'Tuesday'
          when EXTRACT(DAYOFWEEK from DATE(logs.time, 'US/Central')) = 4 then 'Wednesday'
          when EXTRACT(DAYOFWEEK from DATE(logs.time, 'US/Central')) = 5 then 'Thursday'
          when EXTRACT(DAYOFWEEK from DATE(logs.time, 'US/Central')) = 6 then 'Friday'
          when EXTRACT(DAYOFWEEK from DATE(logs.time, 'US/Central')) = 7 then 'Saturday'
          when EXTRACT(DAYOFWEEK from DATE(logs.time, 'US/Central')) = 1 then 'Sunday'
      END AS day_of_week,
      CASE
        WHEN EXTRACT(dayofweek FROM logs.time AT TIME ZONE 'US/Central') in(1,7) THEN 'Weekend'
        ELSE 'Weekday'
      END AS day_type,
      
      CASE
        WHEN logs.url  IS NULL THEN 'Application'
        WHEN logs.url IS NOT NULL AND LOWER(logs.url) like '%search?%' THEN 'Search' -- %Search?% in Templates
        WHEN logs.url IS NOT NULL AND LOWER(logs.url) not like '%search%' THEN 'Site'
      END AS activity_type,
      
CASE 
        WHEN logs.url IS NOT NULL AND LOWER(logs.url) like '%search%' AND (logs.url LIKE '%q=%' OR logs.url LIKE '%text=%'  OR logs.url LIKE '%term=%'  OR logs.url LIKE '%desc=%'  OR logs.url LIKE '%query=%')
        THEN  REGEXP_REPLACE(REGEXP_EXTRACT(logs.url,r'(?:\?|&)(?:(?:[q=|text=|term=|desc=|query=]+)=([^&]*))'),r'[^a-zA-Z]',' ') 
        WHEN logs.url IS NOT NULL AND LOWER(logs.url) like '%search%' AND titlebar LIKE '%Search%' THEN titlebar
        WHEN logs.url IS NOT NULL AND LOWER(logs.url) like '%search%' AND titlebar NOT LIKE '%Search%' THEN NULL
        ELSE NULL
        END AS search_term,

     
      CASE
        WHEN logs.url LIKE 'http://%' THEN SUBSTR(logs.url,8)
        WHEN logs.url LIKE 'https://%' THEN SUBSTR(logs.url,9)
        WHEN logs.url LIKE 'www.%' THEN SUBSTR(logs.url,5) -- Not included in Templates
        ELSE logs.url
      END AS clean_url,

      CASE
        WHEN COALESCE(IF(websiteid IS NOT NULL, productivity, productivity), 0) = 1 THEN 'Approved'
        WHEN COALESCE(IF(websiteid IS NOT NULL, productivity, productivity), 0) = -1 THEN 'Unapproved'
        ELSE 'Pending Approval'
        END AS app_site_approval_status,

      CASE
        WHEN logs.url LIKE 'https://%' THEN SUBSTR(SUBSTR(logs.url,9),0,GREATEST((INSTR(SUBSTR(logs.url,9),'/',1,1)-1),0))
        WHEN logs.url LIKE 'http://%' THEN SUBSTR(SUBSTR(logs.url,8),0,GREATEST((INSTR(SUBSTR(logs.url,8),'/',1,1)-1),0))
        WHEN logs.url LIKE 'www.%' THEN SUBSTR(SUBSTR(logs.url,5),0,GREATEST((INSTR(SUBSTR(logs.url,5),'/',1,1)-1),0))  -- Not included in Templates
        ELSE SUBSTR(logs.url,0,GREATEST((INSTR(logs.url,'/',1,1)-1),0))
      END AS site,
      CASE
        WHEN logs.url IS NULL THEN COALESCE(logs.log_description,logs.executable)
        WHEN logs.url LIKE 'http://%' THEN CONCAT(COALESCE(logs.log_description,logs.executable),'(',SUBSTR(SUBSTR(logs.url,8),0,GREATEST((INSTR(SUBSTR(logs.url,8),'/',1,1)-1),0)),')')
        WHEN logs.url LIKE 'https://%' THEN CONCAT(COALESCE(logs.log_description,logs.executable),'(',SUBSTR(SUBSTR(logs.url,9),0,GREATEST((INSTR(SUBSTR(logs.url,9),'/',1,1)-1),0)),')')
        WHEN logs.url LIKE 'www.%' THEN CONCAT(COALESCE(logs.log_description,logs.executable),'(',SUBSTR(SUBSTR(logs.url,5),0,GREATEST((INSTR(SUBSTR(logs.url,5),'/',1,1)-1),0)),')') -- Not included in Templates
        ELSE CONCAT(COALESCE(logs.log_description,logs.executable),'(',SUBSTR(logs.url,0,GREATEST((INSTR(logs.url,'/',1,1)-1),0)),')')
      END AS browser_site,
      CASE
        WHEN logs.url IS NULL THEN COALESCE(logs.log_description,logs.executable)
        WHEN logs.url LIKE 'http://%' THEN SUBSTR(SUBSTR(logs.url,8),0,GREATEST((INSTR(SUBSTR(logs.url,8),'/',1,1)-1),0))
        WHEN logs.url LIKE 'https://%' THEN SUBSTR(SUBSTR(logs.url,9),0,GREATEST((INSTR(SUBSTR(logs.url,9),'/',1,1)-1),0))
        WHEN logs.url LIKE 'www.%' THEN SUBSTR(SUBSTR(logs.url,5),0,GREATEST((INSTR(SUBSTR(logs.url,5),'/',1,1)-1),0)) -- Not included in Templates
        ELSE SUBSTR(logs.url,0,GREATEST((INSTR(logs.url,'/',1,1)-1),0))
      END AS application_or_site,
      CASE
        WHEN logs.log_description IS NULL THEN FALSE
        WHEN UPPER(logs.log_description) LIKE '%ONEDRIVE%'
          OR UPPER(logs.log_description) LIKE '%DROPBOX%'
          OR UPPER(logs.log_description) LIKE '%FTP%'
          OR UPPER(logs.log_description) LIKE '%SFTP%'
          OR UPPER(logs.log_description) LIKE '%USB%'
          OR UPPER(logs.log_description) LIKE '%DISK%' THEN TRUE
        ELSE FALSE
      END AS potential_file_transfer,
    
      CASE
        WHEN EXTRACT(HOUR FROM TIMESTAMP_TRUNC(logs.time, MINUTE,'US/Central')) >= 6 AND EXTRACT(HOUR FROM TIMESTAMP_TRUNC(logs.time, MINUTE,'US/Central')) < 12 THEN 'Morning'
        WHEN EXTRACT(HOUR FROM TIMESTAMP_TRUNC(logs.time, MINUTE,'US/Central')) >= 12 AND EXTRACT(HOUR FROM TIMESTAMP_TRUNC(logs.time, MINUTE,'US/Central')) < 18 THEN 'Afternoon'
        WHEN EXTRACT(HOUR FROM TIMESTAMP_TRUNC(logs.time, MINUTE,'US/Central')) >= 18 AND EXTRACT(HOUR FROM TIMESTAMP_TRUNC(logs.time, MINUTE,'US/Central')) < 24 THEN 'Night'
        ELSE 'Overnight'
      END AS period_of_day,
      COALESCE(logs.useralias,logs.user) AS user_name,
      DATE_ADD(LAST_DAY(DATE(logs.time, 'US/Central'), WEEK), INTERVAL -6 DAY) AS activity_week,
      CASE
        WHEN logs.email = 1 or logs.popup = 1 or logs.screenshot = 1 or logs.terminate = 1 or logs.webhook =1 THEN 1
        ELSE 0
      END AS alarms_triggered,
      CASE
        WHEN logs.email = 1 THEN 'Email'
        WHEN logs.popup = 1 THEN 'Popup'
        WHEN logs.screenshot >= 1 THEN 'Screenshot'
        WHEN logs.terminate = 1 THEN 'Terminate'
        WHEN logs.webhook = 1 THEN 'Webhook'
      END AS alarm_action,
 '2.0' AS dataset_version,

    FROM
    `activtrak-com-214616.604700.log_raw`
    AS logs

  WHERE logs.user NOT IN('SYSTEM','')
  AND DATE(logs.time, 'US/Central') >= DATE_SUB(CURRENT_DATE('US/Central'), INTERVAL 1 YEAR) -- Always  query on the lasy year data only
  AND (category != 'Ignore' OR category IS NULL) and active_state != 3